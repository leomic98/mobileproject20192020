-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Creato il: Apr 27, 2020 alle 15:34
-- Versione del server: 10.4.10-MariaDB
-- Versione PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alco_db`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `categoria_piatto`
--

CREATE TABLE `categoria_piatto` (
  `id_categoria_piatto` int(255) NOT NULL,
  `Nome` tinytext COLLATE utf8_bin NOT NULL,
  `Descrizione` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `categoria_piatto`
--

INSERT INTO `categoria_piatto` (`id_categoria_piatto`, `Nome`, `Descrizione`) VALUES
(1, 'primo', 'primi piatti'),
(2, 'roll', 'roll'),
(3, 'fritti', 'fritti'),
(4, 'nigiri', 'nigiri'),
(5, 'temaki', 'temaki');

-- --------------------------------------------------------

--
-- Struttura della tabella `ingrediente`
--

CREATE TABLE `ingrediente` (
  `id_ingrediente` int(255) NOT NULL,
  `Nome` tinytext COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `ingrediente`
--

INSERT INTO `ingrediente` (`id_ingrediente`, `Nome`) VALUES
(1, 'riso'),
(2, 'salmone crudo'),
(3, 'tonno crudo'),
(4, 'tofu');

-- --------------------------------------------------------

--
-- Struttura della tabella `ingrediente_in_piatto`
--

CREATE TABLE `ingrediente_in_piatto` (
  `id_ingrediente_in_piatto` int(255) NOT NULL,
  `FK_ingrediente` int(255) NOT NULL,
  `FK_piatto` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `ingrediente_in_piatto`
--

INSERT INTO `ingrediente_in_piatto` (`id_ingrediente_in_piatto`, `FK_ingrediente`, `FK_piatto`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 1, 2),
(4, 3, 2),
(5, 4, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(255) NOT NULL,
  `Nome` tinytext COLLATE utf8_bin NOT NULL,
  `FK_id_ristorante` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `menu`
--

INSERT INTO `menu` (`id_menu`, `Nome`, `FK_id_ristorante`) VALUES
(1, 'All you can eat pranzo', 1),
(2, 'All you can eat cena', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE `ordine` (
  `id_ordine` int(255) NOT NULL,
  `Ordine_txt` text COLLATE utf8_bin NOT NULL,
  `FK_id_tavolo` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `ordine`
--

INSERT INTO `ordine` (`id_ordine`, `Ordine_txt`, `FK_id_tavolo`) VALUES
(3, '', 4);

-- --------------------------------------------------------

--
-- Struttura della tabella `piatto`
--

CREATE TABLE `piatto` (
  `id_piatto` int(255) NOT NULL,
  `Nome` tinytext COLLATE utf8_bin NOT NULL,
  `FK_id_categoria_piatto` int(255) NOT NULL,
  `Img` tinytext COLLATE utf8_bin DEFAULT NULL,
  `Prezzo` float NOT NULL,
  `Quantita` int(11) NOT NULL,
  `Disponibile` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `piatto`
--

INSERT INTO `piatto` (`id_piatto`, `Nome`, `FK_id_categoria_piatto`, `Img`, `Prezzo`, `Quantita`, `Disponibile`) VALUES
(1, 'nigiri salmone', 4, '/home/luca//home/luca/Pictures/android.png\r\n\r\n\r\n\r\n', 5, 1, 1),
(2, 'nigiri tonno', 4, '/home/luca//home/luca/Pictures/android.png', 5, 1, 1),
(3, 'tofu fritto', 3, '/home/luca//home/luca/Pictures/android.png', 5, 4, 1),
(4, 'temaki sake', 5, NULL, 4, 1, 1),
(5, 'temaki tonno', 5, NULL, 3, 1, 1),
(6, 'temaki tonno', 5, NULL, 3, 1, 1),
(7, 'salmon roll', 2, NULL, 4, 2, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `piatto_in_menu`
--

CREATE TABLE `piatto_in_menu` (
  `id_piatto_in_menu` int(255) NOT NULL,
  `FK_id_menu` int(255) NOT NULL,
  `FK_id_piatto` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `piatto_in_menu`
--

INSERT INTO `piatto_in_menu` (`id_piatto_in_menu`, `FK_id_menu`, `FK_id_piatto`) VALUES
(1, 1, 3),
(2, 1, 1),
(3, 1, 2),
(4, 2, 1),
(5, 1, 4),
(6, 1, 6),
(7, 1, 7);

-- --------------------------------------------------------

--
-- Struttura della tabella `piatto_in_ordine`
--

CREATE TABLE `piatto_in_ordine` (
  `id_piatto_in_ordine` int(255) NOT NULL,
  `FK_id_piatto` int(255) NOT NULL,
  `FK_id_ordine` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `ristorante`
--

CREATE TABLE `ristorante` (
  `id_ristorante` int(255) NOT NULL,
  `pwd` varchar(255) COLLATE utf8_bin NOT NULL,
  `usr_name` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `ristorante`
--

INSERT INTO `ristorante` (`id_ristorante`, `pwd`, `usr_name`) VALUES
(1, 'poop', 'butterfly');

-- --------------------------------------------------------

--
-- Struttura della tabella `tavolo`
--

CREATE TABLE `tavolo` (
  `id_tavolo` int(255) NOT NULL,
  `Prenotato` tinyint(4) DEFAULT NULL,
  `N_posti` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dump dei dati per la tabella `tavolo`
--

INSERT INTO `tavolo` (`id_tavolo`, `Prenotato`, `N_posti`) VALUES
(1, 0, 3),
(2, 0, 3),
(3, 0, 3),
(4, 0, 3),
(5, 0, 3),
(6, 0, 3),
(7, 0, 3);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `categoria_piatto`
--
ALTER TABLE `categoria_piatto`
  ADD PRIMARY KEY (`id_categoria_piatto`);

--
-- Indici per le tabelle `ingrediente`
--
ALTER TABLE `ingrediente`
  ADD PRIMARY KEY (`id_ingrediente`);

--
-- Indici per le tabelle `ingrediente_in_piatto`
--
ALTER TABLE `ingrediente_in_piatto`
  ADD PRIMARY KEY (`id_ingrediente_in_piatto`),
  ADD KEY `FK_ingrediente` (`FK_ingrediente`),
  ADD KEY `FK_piatto` (`FK_piatto`);

--
-- Indici per le tabelle `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `FK_id_ristorante` (`FK_id_ristorante`);

--
-- Indici per le tabelle `ordine`
--
ALTER TABLE `ordine`
  ADD PRIMARY KEY (`id_ordine`),
  ADD KEY `FK_id_tavolo` (`FK_id_tavolo`);

--
-- Indici per le tabelle `piatto`
--
ALTER TABLE `piatto`
  ADD PRIMARY KEY (`id_piatto`),
  ADD KEY `FK_id_categoria_piatto` (`FK_id_categoria_piatto`);

--
-- Indici per le tabelle `piatto_in_menu`
--
ALTER TABLE `piatto_in_menu`
  ADD PRIMARY KEY (`id_piatto_in_menu`),
  ADD KEY `FK_id_menu` (`FK_id_menu`),
  ADD KEY `FK_id_piatto` (`FK_id_piatto`);

--
-- Indici per le tabelle `piatto_in_ordine`
--
ALTER TABLE `piatto_in_ordine`
  ADD PRIMARY KEY (`id_piatto_in_ordine`),
  ADD KEY `FK_id_piatto` (`FK_id_piatto`),
  ADD KEY `FK_id_ordine` (`FK_id_ordine`);

--
-- Indici per le tabelle `ristorante`
--
ALTER TABLE `ristorante`
  ADD PRIMARY KEY (`id_ristorante`);

--
-- Indici per le tabelle `tavolo`
--
ALTER TABLE `tavolo`
  ADD PRIMARY KEY (`id_tavolo`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `categoria_piatto`
--
ALTER TABLE `categoria_piatto`
  MODIFY `id_categoria_piatto` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `ingrediente`
--
ALTER TABLE `ingrediente`
  MODIFY `id_ingrediente` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT per la tabella `ingrediente_in_piatto`
--
ALTER TABLE `ingrediente_in_piatto`
  MODIFY `id_ingrediente_in_piatto` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `ordine`
--
ALTER TABLE `ordine`
  MODIFY `id_ordine` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `piatto`
--
ALTER TABLE `piatto`
  MODIFY `id_piatto` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `piatto_in_menu`
--
ALTER TABLE `piatto_in_menu`
  MODIFY `id_piatto_in_menu` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `piatto_in_ordine`
--
ALTER TABLE `piatto_in_ordine`
  MODIFY `id_piatto_in_ordine` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `ristorante`
--
ALTER TABLE `ristorante`
  MODIFY `id_ristorante` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `tavolo`
--
ALTER TABLE `tavolo`
  MODIFY `id_tavolo` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `ingrediente_in_piatto`
--
ALTER TABLE `ingrediente_in_piatto`
  ADD CONSTRAINT `ingrediente_in_piatto_ibfk_2` FOREIGN KEY (`FK_piatto`) REFERENCES `piatto` (`id_piatto`),
  ADD CONSTRAINT `ingrediente_in_piatto_ibfk_3` FOREIGN KEY (`FK_ingrediente`) REFERENCES `ingrediente` (`id_ingrediente`);

--
-- Limiti per la tabella `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`FK_id_ristorante`) REFERENCES `ristorante` (`id_ristorante`);

--
-- Limiti per la tabella `ordine`
--
ALTER TABLE `ordine`
  ADD CONSTRAINT `ordine_ibfk_1` FOREIGN KEY (`FK_id_tavolo`) REFERENCES `tavolo` (`id_tavolo`);

--
-- Limiti per la tabella `piatto`
--
ALTER TABLE `piatto`
  ADD CONSTRAINT `piatto_ibfk_1` FOREIGN KEY (`FK_id_categoria_piatto`) REFERENCES `categoria_piatto` (`id_categoria_piatto`);

--
-- Limiti per la tabella `piatto_in_menu`
--
ALTER TABLE `piatto_in_menu`
  ADD CONSTRAINT `piatto_in_menu_ibfk_1` FOREIGN KEY (`FK_id_menu`) REFERENCES `menu` (`id_menu`),
  ADD CONSTRAINT `piatto_in_menu_ibfk_2` FOREIGN KEY (`FK_id_piatto`) REFERENCES `piatto` (`id_piatto`);

--
-- Limiti per la tabella `piatto_in_ordine`
--
ALTER TABLE `piatto_in_ordine`
  ADD CONSTRAINT `piatto_in_ordine_ibfk_1` FOREIGN KEY (`FK_id_ordine`) REFERENCES `ordine` (`id_ordine`),
  ADD CONSTRAINT `piatto_in_ordine_ibfk_2` FOREIGN KEY (`FK_id_piatto`) REFERENCES `piatto` (`id_piatto`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
