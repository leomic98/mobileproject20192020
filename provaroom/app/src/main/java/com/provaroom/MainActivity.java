package com.provaroom;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.provaroom.database.Dish;
import com.provaroom.database.DishViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    DishViewModel dishViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.textView);
        dishViewModel = new ViewModelProvider(this).get(DishViewModel.class);

        final EditText idEditText = findViewById(R.id.idEditText);
        final EditText nameEditText = findViewById(R.id.nameEditText);
        final EditText imageEditText = findViewById(R.id.imageEditText);
        final EditText priceEditText = findViewById(R.id.priceEditText);
        final EditText quantityEditText = findViewById(R.id.assdasd);
        final EditText categoryEditText = findViewById(R.id.categoryEditText);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int dishId = Integer.parseInt(idEditText.getText().toString());
                String dishName = nameEditText.getText().toString();
                String dishImage = imageEditText.getText().toString();
                int dishPrice = Integer.parseInt(priceEditText.getText().toString());
                int dishQuantity = Integer.parseInt(quantityEditText.getText().toString());
                String  categoryName = categoryEditText.getText().toString();
                if (!dishName.isEmpty() && !dishImage.isEmpty() && !categoryName.isEmpty()){
                    dishViewModel.addDish(new Dish(dishId, dishName, categoryName, dishImage, dishPrice, dishQuantity));
                    idEditText.getText().clear();
                    nameEditText.setText("");
                    imageEditText.setText("");
                    priceEditText.getText().clear();
                    quantityEditText.getText().clear();
                    categoryEditText.setText("");
                }
            }
        });

        dishViewModel.getDishes().observe(this, new Observer<List<Dish>>() {
            @Override
            public void onChanged(List<Dish> dishes) {
                List < String > list = new ArrayList<>();
                for (Dish dish: dishes){
                    list.add(dish.getName());
                }
                textView.setText(list.toString());
            }
        });
    }
}
