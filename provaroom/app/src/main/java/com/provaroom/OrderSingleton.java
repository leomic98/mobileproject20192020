package com.provaroom;

import java.util.HashMap;
import java.util.Map;

public class OrderSingleton {

    private static OrderSingleton istance = null;
    private Map<Integer, Integer> order;

    public OrderSingleton(){
        this.order = new HashMap<>();
    }

    public static  OrderSingleton getInstance(){
        if (istance == null){
            synchronized (MenuSingleton.class) {
                if (istance == null) {
                    istance = new OrderSingleton();
                }
            }
        }
        return istance;
    }

    public void addToOrder(Integer id, Integer quantity){
        if(order.values().contains(id)) {
            Integer actualQuantity = order.get(id);
            actualQuantity += quantity;
            order.put(id, actualQuantity);
        }else {
            order.put(id,quantity);
        }
    }

    public Map<Integer,Integer> getOrder(){
        return this.order;
    }
}
