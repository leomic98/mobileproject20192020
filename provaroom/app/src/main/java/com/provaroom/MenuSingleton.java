package com.provaroom;

import com.provaroom.database.Dish;

import java.util.ArrayList;
import java.util.List;

public class MenuSingleton {

    private static MenuSingleton istance = null;

    private List<Dish> dishes;

    private MenuSingleton(){
        this.dishes = new ArrayList<>();
    }

    public static  MenuSingleton getInstance(){
        if (istance == null){
            synchronized (MenuSingleton.class) {
                if (istance == null) {
                    istance = new MenuSingleton();
                }
            }
        }
        return istance;
    }

    //return true if the category is present in the list
    public boolean isAlreadyPresent(String categoryName){
        for (Dish dish: dishes){
            if(dish.getCategoryName().equals(categoryName)){
                return true;
            }
        }
        return false;
    }

    //add a list of dishes
    public void addDishes(List<Dish> dishes){
        this.dishes.addAll(dishes);
    }

    //returns all dishes in a certain category
    public List<Dish> getDishesInCategory(String categoryName){
        List<Dish> dishesInCategory = new ArrayList<>();
        for (Dish dish : dishes){
            if (dish.getCategoryName().equals(categoryName)){
                dishesInCategory.add(dish);
            }
        }
        return dishesInCategory;
    }
}
