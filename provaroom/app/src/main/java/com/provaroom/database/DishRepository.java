package com.provaroom.database;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class DishRepository {

    private DishDAO dishDAO;
    private LiveData<List<Dish>> dishes;

    public DishRepository(Application application){
        DishRoomDatabase db = DishRoomDatabase.getDatabase(application);
        dishDAO=db.dishDAO();
        dishes = dishDAO.getDishes();
    }

    public LiveData<List<Dish>> getDishes(){
        return dishes;
    }


    public void addDish(final Dish dish){
        DishRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                dishDAO.addDish(dish);
            }
        });
    }
}
