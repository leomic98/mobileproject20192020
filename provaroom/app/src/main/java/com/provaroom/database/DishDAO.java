package com.provaroom.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DishDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addDish(Dish dish);

    @Query("SELECT * FROM dish_saved ORDER BY dish_id")
    LiveData<List<Dish>> getDishes();

    @Query("SELECT * FROM dish_saved WHERE dish_id LIKE :id")
    Dish findDishByID(int id);
}
