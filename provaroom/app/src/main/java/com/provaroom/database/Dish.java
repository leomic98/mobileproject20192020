package com.provaroom.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "dish_saved")
public class Dish {
    @PrimaryKey
    @ColumnInfo(name="dish_id")
    private int id;

    @ColumnInfo(name = "dish_name")
    private String name;

    @ColumnInfo(name="category_name")
    private String categoryName;

    @ColumnInfo(name = "img")
    private String image;

    @ColumnInfo(name="price")
    private int price;

    @ColumnInfo(name = "quantity")
    private int quantity;

    public Dish(int id, String name, String categoryName, String image, int price, int quantity){
        this.id = id;
        this.name = name;
        this.categoryName = categoryName;
        this.image = image;
        this.price = price;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getImage() {
        return image;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

}
