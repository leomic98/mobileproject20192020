package com.provaroom.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Dish.class},version = 1, exportSchema = false)
abstract class DishRoomDatabase extends RoomDatabase {

    abstract  DishDAO dishDAO();

    //singleton instance
    private static volatile DishRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS=4;

    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static DishRoomDatabase getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (DishRoomDatabase.class){
                if (INSTANCE==null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            DishRoomDatabase.class, "dish_database").build();
                }
            }
        }
    return INSTANCE;
    }
}
