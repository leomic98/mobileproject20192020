package com.provaroom.database;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class DishViewModel extends AndroidViewModel {

    private DishRepository repository;
    private LiveData<List<Dish>> dishes;

    public DishViewModel(@NonNull Application application){
        super(application);
        repository = new DishRepository(application);
        dishes = repository.getDishes();
    }

    public LiveData<List<Dish>> getDishes(){
        return dishes;
    }

    public void addDish(Dish dish){
        repository.addDish(dish);
    }
}
