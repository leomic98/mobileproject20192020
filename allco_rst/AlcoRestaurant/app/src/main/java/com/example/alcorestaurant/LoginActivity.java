package com.example.alcorestaurant;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.VolleyError;
import com.example.alcorestaurant.volleyservice.IResult;
import com.example.alcorestaurant.volleyservice.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText usrEditText = findViewById(R.id.usrEditText);
        final EditText pwdEditText = findViewById(R.id.pwdEditText);

        findViewById(R.id.loginButton).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        if(usrEditText.getText().toString().length() > 0 && pwdEditText.getText().toString().length() > 0) {
                            try {
                                loginRequest(usrEditText.getText().toString(), pwdEditText.getText().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
    }

    private void loginRequest(String usr, String pwd) throws JSONException {
        String url = "https://allco.altervista.org/login.php";

        JSONObject jBody = new JSONObject();
        jBody.put("usr", usr);
        jBody.put("pwd", pwd);

        VolleyService mVolleyService = new VolleyService(new IResult() {
            @Override
            public void notifyJASuccess(JSONArray response) {

            }

            @Override
            public void notifyJOSuccess(JSONObject response) {
                try {
                    JSONObject jo = response;
                    String result = jo.getString("response");
                    if (result.equals("ok")){
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
                //shouldn't be here
                showToast("Username o password errati");
            }
        } ,this);

        mVolleyService.postJSONObjectVolley(url, jBody);
    }

    private void showToast(final String msg){
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(this, msg, duration);
        toast.show();
    }
}
