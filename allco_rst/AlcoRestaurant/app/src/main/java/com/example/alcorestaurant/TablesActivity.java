package com.example.alcorestaurant;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class TablesActivity extends AppCompatActivity implements TablesManager{
    private static final String BACK_STACK_ROOT_TAG = "root_fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tables);

        swapFragment(new TablesFragment(), false);
    }

    @Override
    public void swapFragment(Fragment newFragment, boolean isBackStack){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack("BACKSTACK_BASE", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        transaction.replace(R.id.tablesFragmentContainer, newFragment);
        if(isBackStack){
            transaction.addToBackStack("BACKSTACK_BASE");
        }
        transaction.commit();
    }
}
