package com.example.alcorestaurant;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Map;

public class TablesAdapter extends RecyclerView.Adapter<TablesAdapter.TablesViewHolder>{
    public Map<Integer, String> mDataSet;
    private  TablesManager tablesManager;

    /**
     * ViewHolder class to adapt for Tables data objects.
     */
    public static class TablesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public Button btn;
        private TablesManager tablesManager;
        public String tableName;

        public TablesViewHolder(Button b, TablesManager tablesManager) {
            super(b);
            this.btn = b;
            this.tablesManager = tablesManager;
            this.tableName = " ";

            btn.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            tablesManager.swapFragment(new SelectedTableFragment(
                    Integer.parseInt(btn.getText().toString()), tableName), true);
        }
    }

    public TablesAdapter(Map<Integer, String> dataSet, TablesManager tablesManager){
        this.mDataSet = dataSet;
        this.tablesManager = tablesManager;
    }

    @NonNull
    @Override
    public TablesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Button b = (Button) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.table_button_layout, parent, false);

        TablesViewHolder vh = new TablesViewHolder(b, tablesManager);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull TablesViewHolder holder, int position) {
        Integer tableID = (Integer) mDataSet.keySet().toArray()[position];
        holder.btn.setText(tableID.toString());
        holder.tableName = (new StringBuilder().append(holder.tableName).append(mDataSet.get(tableID)).toString());

        Log.d("TAG", "onBindViewHolder: "+holder.tableName);
    }
    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}
