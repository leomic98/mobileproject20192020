package com.example.alcorestaurant;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.List;

public class DishAdapter extends RecyclerView.Adapter<DishAdapter.DishViewHolder> {
    private List<Dish> dataSet;
    private DishManager dishManager;

    public static class DishViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public DishManager dishManager;
        public TextView tView;
        public Button btn;
        public ImageView dishImageView;

        public DishViewHolder(@NonNull View itemView, DishManager dishManager) {
            super(itemView);
            this.tView = (TextView) itemView.findViewById(R.id.dishNameTextView);
            this.btn = (Button) itemView.findViewById(R.id.removeDishButton);
            this.dishImageView = itemView.findViewById(R.id.dishImageView);
            this.dishManager = dishManager;

            btn.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(btn.getText().equals("+")){
                try {
                    dishManager.setDishAvailability(getAdapterPosition(),true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                btn.setText("X");
            } else {
                try {
                    dishManager.setDishAvailability(getAdapterPosition(),false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                btn.setText("+");
            }
        }
    }

    public DishAdapter(List<Dish> nDataSet, DishManager dishManager){
        this.dataSet = nDataSet;
        this.dishManager = dishManager;
    }

    @NonNull
    @Override
    public DishViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View vg =  LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.dish_card, parent, false);
            DishViewHolder dv = new DishViewHolder(vg, dishManager);
            return dv;
    }

    @Override
    public void onBindViewHolder(@NonNull DishViewHolder holder, int position) {
        Dish dish = dataSet.get(position);
        holder.tView.setText(dish.getName());
        Picasso.get()
                .load(dish.getImage())
                .resize(100, 100)
                .centerCrop()
                .into(holder.dishImageView);
        holder.btn.setText(dish.isFinished() ? "+" : "X");
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
