package com.example.alcorestaurant;

import androidx.fragment.app.Fragment;

public interface TablesManager {

    /**
     * Swap current fragment with the provided new fragment.
     *
     * @param fragment new fragment to be visualized
     */
    void swapFragment(Fragment fragment, boolean isBackStack);
}
