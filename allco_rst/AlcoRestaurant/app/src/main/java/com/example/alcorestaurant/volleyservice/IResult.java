package com.example.alcorestaurant.volleyservice;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONObject;

public interface IResult {

    void notifyJASuccess(JSONArray response);
    void notifyJOSuccess(JSONObject response);
    void notifyError(VolleyError error);
}
