package com.example.alcorestaurant;

import org.json.JSONException;

public interface DishManager {

    void setDishAvailability(int position, boolean isAvailable) throws JSONException;
}
