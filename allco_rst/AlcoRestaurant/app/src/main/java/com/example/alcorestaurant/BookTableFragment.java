package com.example.alcorestaurant;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.alcorestaurant.volleyservice.IResult;
import com.example.alcorestaurant.volleyservice.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Optional;

/**
 * Used to hard-create a table to be displayed in @TablesFragment, even if the table order is still empty
 */
public class BookTableFragment extends Fragment {
    private RequestQueue requestQueue;
    private final static String TAG = "ALLCO_SERVER_REQUEST";
    private TablesManager tablesManager;

    /**
     * Init tablesManager for communicating events with parent activity
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            tablesManager = (TablesManager) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement TablesManager");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v= (ViewGroup) inflater.inflate(
                R.layout.fragment_book_table, container, false);

        final EditText tableIdText = v.findViewById(R.id.numberEditText);
        final EditText numPersonText = v.findViewById(R.id.numPersonEditText);
        final EditText namePersonText = v.findViewById(R.id.nameEditText);

        v.findViewById(R.id.bookButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tableIdText.getText().toString().length() > 0 && numPersonText.getText().toString().length() > 0) {
                    try {
                        int tNum = Integer.parseInt(tableIdText.getText().toString());
                        int pNum = Integer.parseInt(numPersonText.getText().toString());
                        if(namePersonText.getText().toString().length() > 0){
                            String name = namePersonText.getText().toString();
                            bookTable(tNum, pNum, name);
                        } else {
                            bookTable(tNum, pNum, "");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        return v;
    }

    /**
     * Books a table in order to dipslay it in @TablesFragment
     *
     * @param table_id
     * @param num_person
     * @param name
     * @throws JSONException
     */
    private void bookTable(int table_id, int num_person, String name) throws JSONException {
        String url = "https://allco.altervista.org/book_table.php";

        //Request params
        JSONObject jBody = new JSONObject();
        jBody.put("table_id", table_id);
        jBody.put("quantity", num_person);
        jBody.put("name", name);

        VolleyService mVolleyService = new VolleyService(new IResult() {
            @Override
            public void notifyJASuccess(JSONArray response) { }

            @Override
            public void notifyJOSuccess(JSONObject response) {
                tablesManager.swapFragment(new TablesFragment(), false);
                Log.d(TAG, "onResponse: "+response.toString());
            }

            @Override
            public void notifyError(VolleyError error) {
                Log.d(TAG, "Error");
                error.printStackTrace();
            }
        } ,getContext());
        mVolleyService.postJSONObjectVolley(url, jBody);
    }
}
