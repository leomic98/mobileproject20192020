package com.example.alcorestaurant;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.example.alcorestaurant.volleyservice.IResult;
import com.example.alcorestaurant.volleyservice.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Displays every dish in every menu, can manage dish availability on-the-go
 */
public class MenuActivity extends AppCompatActivity implements DishManager{
   private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<Dish> dataSet = new ArrayList<>();
    private final static String TAG = "MENU_ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        downloadDishes();
        initRecyclerView();
    }

    private void initRecyclerView(){
        recyclerView = (RecyclerView) findViewById(R.id.dishesRecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new DishAdapter(dataSet, this);
        recyclerView.setAdapter(mAdapter);
    }

    /**
     * Downloads dishes from server
     */
    private void downloadDishes() {
        String url = "https://allco.altervista.org/get_all_dishes.php";

        VolleyService mVolleyService = new VolleyService(new IResult() {
            @Override
            public void notifyJASuccess(JSONArray response) {
                try {
                    for(int i=0 ; i<response.length() ; i++){
                        JSONObject joDish = response.getJSONObject(i);
                        Dish dish = new Dish(joDish.getInt("id"),
                                joDish.getString("name"), joDish.getString("image"));
                        if(Integer.parseInt(joDish.getString("availability")) == 0){
                            dish.setFinished(true);
                        }
                        // Check for double elements
                        if(!dataSet.contains(dish)){
                            dataSet.add(dish);
                            mAdapter.notifyItemInserted(dataSet.size()-1);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyJOSuccess(JSONObject response) { }

            @Override
            public void notifyError(VolleyError error) {
                Log.d(TAG, "Error");
                error.printStackTrace();
            }
        } , this);
        mVolleyService.getJSONArrayVolley(url);
    }

    @Override
    public void setDishAvailability(int position, boolean isAvailable) throws JSONException {
        if(isAvailable){
            dataSet.get(position).setFinished(false);
            notifyAvailabilityToServer(dataSet.get(position).getDishID(), 1);
        } else {
            dataSet.get(position).setFinished(true);
            notifyAvailabilityToServer(dataSet.get(position).getDishID(), 0);
        }
    }

    /**
     * Set dish availability in the server
     * @param dishID
     * @param available
     * @throws JSONException
     */
    private void notifyAvailabilityToServer(int dishID, int available) throws JSONException {
        String url = "https://allco.altervista.org/set_dish_availability.php";

        JSONObject jBody = new JSONObject();
        jBody.put("dish_id", dishID);
        jBody.put("avail", available);

        VolleyService mVolleyService = new VolleyService(new IResult() {
            @Override
            public void notifyJASuccess(JSONArray response) {
            }

            @Override
            public void notifyJOSuccess(JSONObject response) { }

            @Override
            public void notifyError(VolleyError error) {
                Log.d(TAG, "Error");
                error.printStackTrace();
            }
        } , this);
        mVolleyService.postJSONObjectVolley(url, jBody);
    }
}
