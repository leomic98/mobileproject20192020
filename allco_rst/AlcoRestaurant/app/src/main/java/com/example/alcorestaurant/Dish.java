package com.example.alcorestaurant;

public class Dish {

    private int dishID;
    private String name;
    private String image;
    private boolean finished;

    public Dish(int dishID, String name, String image){
        this.dishID = dishID;
        this.name = name;
        this.image = image;
        //By default a dish is not finished when added
        this.finished = false;
    }

    public String getName(){
        return this.name;
    }

    public int getDishID(){ return this.dishID; }

    public boolean isFinished(){
        return this.finished;
    }

    public void setFinished(boolean finished){
        this.finished = finished;
    }

    public String getImage() {
        return image;
    }
}
