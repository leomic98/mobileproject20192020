package com.example.alcorestaurant;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.example.alcorestaurant.volleyservice.IResult;
import com.example.alcorestaurant.volleyservice.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages the selected table
 */
public class SelectedTableFragment extends Fragment {
    private String tableName;
    private int tableID;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private TextView tableTextView;
    private TextView nameTextView;
    private Map<Integer, String> dataSet = new HashMap<>();
    private final static String TAG = "ALLCO_SERVER_REQUEST";
    private TablesManager tablesManager;

    public SelectedTableFragment(Integer id, String name){
        this.tableName = name;
        this.tableID = id;
    }

    /**
     * Init tablesManager for communicating events with parent activity
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            tablesManager = (TablesManager) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement TablesManager");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v= (ViewGroup) inflater.inflate(
                R.layout.fragment_selected_table, container, false);

        tableTextView = v.findViewById(R.id.tableNumberView);
        tableTextView.append(" "+String.valueOf(tableID));
        nameTextView = v.findViewById(R.id.nameTextView);
        if(!TextUtils.isEmpty(tableName)){
            nameTextView.setText(tableName);
        }

        v.findViewById(R.id.clearTableButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                freeTable();
            }
        });

        downloadOrders();
        initRecyclerView(v);
        return v;
    }

    private void initRecyclerView(View v){
        recyclerView = (RecyclerView) v.findViewById(R.id.ordersRecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(v.getContext());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new OrdersAdapter(dataSet);
        recyclerView.setAdapter(mAdapter);
    }

    /**
     * Downloads orders from server with given tableID
     */
    private void downloadOrders(){
        String url = "https://allco.altervista.org/get_orders.php?table_id="+tableID;

        VolleyService mVolleyService = new VolleyService(new IResult() {
            @Override
            public void notifyJASuccess(JSONArray response) {
                try {
                    for(int i=0 ; i<response.length() ; i++){
                        JSONObject jo = response.getJSONObject(i);
                        String order = jo.getString("order");
                        Log.d(TAG, "response: "+response.toString());
                        // Check for double elements
                        if(!dataSet.containsValue(order)){
                            dataSet.put(i, order);
                            mAdapter.notifyItemInserted(i);
                            Log.d(TAG, "notifyJASuccess: dataSet: "+dataSet.toString());
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyJOSuccess(JSONObject response) { }

            @Override
            public void notifyError(VolleyError error) {
                Log.d(TAG, "Error");
                error.printStackTrace();
            }
        } ,getContext());
        mVolleyService.getJSONArrayVolley(url);
    }

    /**
     * Deletes every order from table
     */
    private void freeTable(){
        String url = "https://allco.altervista.org/close_order.php?table_id="+tableID;

        VolleyService mVolleyService = new VolleyService(new IResult() {
            @Override
            public void notifyJASuccess(JSONArray response) { }

            @Override
            public void notifyJOSuccess(JSONObject response) {
                tablesManager.swapFragment(new TablesFragment(), false);
                Log.d(TAG, response.toString());
            }

            @Override
            public void notifyError(VolleyError error) {
                Log.d(TAG, "Error");
                error.printStackTrace();
            }
        } ,getContext());
        mVolleyService.getJSONObjectVolley(url);
    }
}
