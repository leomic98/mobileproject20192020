package com.example.alcorestaurant;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.example.alcorestaurant.volleyservice.IResult;
import com.example.alcorestaurant.volleyservice.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Shows every table that have at least one order
 */
public class TablesFragment extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private Map<Integer, String> dataSet = new HashMap<>();
    private TablesManager tablesManager;

    private final static String TAG = "REQUEST_TABLES_FRAGMENT";

    /**
     * Init tablesManager for communicating events with parent activity
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            tablesManager = (TablesManager) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement TablesManager");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v= (ViewGroup) inflater.inflate(
                R.layout.fragment_tables, container, false);

        v.findViewById(R.id.occupyTableButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tablesManager.swapFragment(new BookTableFragment(), true);
            }
        });

        downloadTables();

        initRecyclerView(v);
        return v;
    }

    private void initRecyclerView(View v){
        recyclerView = (RecyclerView) v.findViewById(R.id.tablesRecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(v.getContext(), 3);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new TablesAdapter(dataSet, tablesManager);
        recyclerView.setAdapter(mAdapter);
    }

    /**
     * Downloads tables dynamically from server
     */
    private void downloadTables(){
        String url = "https://allco.altervista.org/get_occupied_tables.php";

        VolleyService mVolleyService = new VolleyService(new IResult() {
            @Override
            public void notifyJASuccess(JSONArray response) {
                try {
                    for(int i=0 ; i<response.length() ; i++){
                        JSONObject jo = response.getJSONObject(i);
                        // Check for double elements
                        if(!dataSet.containsKey(jo.getInt("id"))){ //Could possibly be uneffective
                            dataSet.put(jo.getInt("id"), jo.getString("name"));
                            mAdapter.notifyItemInserted(dataSet.size()-1);
                        }
                    }
                    Log.d("TAG", "onResponse: "+dataSet.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyJOSuccess(JSONObject response) { }

            @Override
            public void notifyError(VolleyError error) {
                Log.d(TAG, "Error");
                error.printStackTrace();
            }
        } ,getContext());
        mVolleyService.getJSONArrayVolley(url);
    }
}
