package com.example.alcorestaurant;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Map;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.OrdersViewHolder> {
    private Map<Integer, String> dataSet;

    public OrdersAdapter(Map<Integer, String> nDataSet){
        this.dataSet = nDataSet;
    }

    public static class OrdersViewHolder extends RecyclerView.ViewHolder{
        public TextView tView;

        public OrdersViewHolder(@NonNull TextView itemView) {
            super(itemView);
            this.tView = itemView;
        }
    }

    @NonNull
    @Override
    public OrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView txt = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_layout, parent, false);

        OrdersViewHolder ov = new OrdersViewHolder(txt);
        return ov;
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersViewHolder holder, int position) {
        holder.tView.setText(dataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
