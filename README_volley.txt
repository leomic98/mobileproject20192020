Le richieste volley sono state fatte in un progetto a parte
(allco_usr_requests) e lì raccolte in una sola activity.
Tuttavia per poterle eseguire in un qualunque Fragment/Activity 
del main project è sufficiente copiare il metodo della richiesta ed
incollarlo, facendo anche i relativi import delle classi di cui fa
uso la richiesta specifica.