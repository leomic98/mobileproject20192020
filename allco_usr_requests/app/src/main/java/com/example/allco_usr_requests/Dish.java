package com.example.allco_usr_requests;

public class Dish {
    private int dishID;
    private String name;

    public Dish(int dishID, String name) {
        this.dishID = dishID;
        this.name = name;
    }

    public int getDishID() {
        return dishID;
    }

    public String getName() {
        return name;
    }
}
