package com.example.allco_usr_requests;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothGattDescriptor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private RequestQueue requestQueue;
    private final static String TAG = "ALLCO_USR_REQUEST";

    //Table ID to check
    private int tableID = 1;
    //A list containing all menu types
    private Map<Integer, String> menuTypes = new HashMap<>();

    private Map<Integer, String> categories = new HashMap<>();
    //Represents an order by coupling "plate_id":"quantity" into a map
    private Map<Integer, Integer> order = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Some test order
        order.put(1, 10);
        order.put(2, 15);
        order.put(3, 20);

        findViewById(R.id.checkTableButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkTableID(tableID);
            }
        });

        findViewById(R.id.menuTypesButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateMenuTypes(tableID);
            }
        });

        findViewById(R.id.dishesButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePlates(2, 1);
            }
        });

        findViewById(R.id.sendOrderButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    sendOrder(tableID);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (requestQueue != null) {
            requestQueue.cancelAll(TAG);
        }
    }

    /**
     * Used to check if the Table at tableID exists
     * @param id    The Table ID
     */
    private void checkTableID(int id){
        requestQueue = Volley.newRequestQueue(this);
        String url = "https://allco.000webhostapp.com/check_table.php?table_id="+id;

        Log.d(TAG, "checkTableID: "+id);

        StringRequest stringRequest = new StringRequest
                (Request.Method.GET, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, error.toString());
                    }
                });
        stringRequest.setTag(TAG);
        requestQueue.add(stringRequest);
    }

    /**
     * Updates the menu types data set, represented by a List<String>
     */
    private void updateMenuTypes(int tableID){
        requestQueue = Volley.newRequestQueue(this);
        String url = "https://allco.000webhostapp.com/get_menu.php?table_id="+tableID;

        Log.d(TAG, "updateMenuTypes: ");

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for(int i = 0; i < response.length() ; i++){
                                JSONObject jo = response.getJSONObject(i);
                                Integer menuID = jo.getInt("menu_id");
                                String menuType = jo.getString("type");
                                if(!menuTypes.containsKey(menuID)){
                                    menuTypes.put(menuID, menuType);
                                }
                            }
                            Log.d(TAG, "onResponseMenuTypes: "+menuTypes.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        jsonArrayRequest.setTag(TAG);
        requestQueue.add(jsonArrayRequest);
    }

    /**
     * Gets the categories from a specified menu
     * @param menuID    Id of the menu from which get the category
     */
    private void updateCategories(int menuID){
        requestQueue = Volley.newRequestQueue(this);
        String url = "https://allco.000webhostapp.com/only_categories.php?menu_id="+menuID;

        Log.d(TAG, "updateCategories: ");

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for(int i = 0; i < response.length() ; i++){
                                JSONObject jo = response.getJSONObject(i);
                                Integer catID = jo.getInt("category_id");
                                String catName = jo.getString("name");
                                if(!categories.containsKey(catID)){
                                    categories.put(catID, catName);
                                }
                            }
                            Log.d(TAG, "onResponseCategories: "+categories.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        jsonArrayRequest.setTag(TAG);
        requestQueue.add(jsonArrayRequest);
    }

    /**
     * Updates the dishes data set
     */
    private void updatePlates(int categoryID, int menuID){
        requestQueue = Volley.newRequestQueue(this);
        String url = "https://allco.000webhostapp.com/get_only_dishes.php?category_id="+categoryID+"&menu_id="+menuID;

        Log.d(TAG, "updatePlates: ");

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            JSONObject jo = response.getJSONObject(0);
                            Log.d(TAG, "onResponsePlates: "+jo.toString() );
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        jsonArrayRequest.setTag(TAG);
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonArrayRequest);
    }

    /**
     * Send the order to the server.
     * An order is represented by a Map<Integer, Integer> where the firs int is the
     * dish id, whereas the second is the quantity ordered.
     * @throws JSONException
     */
    private void sendOrder(int tableID) throws JSONException {
        requestQueue = Volley.newRequestQueue(this);
        String url = "https://allco.000webhostapp.com/order.php";




        //Add tableID to request body
        JSONObject jBody = new JSONObject();
        jBody.put("table_id", 4);

        //Array containing the order
        JSONArray orderHolder = new JSONArray();
        //Add the order to orderHolder
        for(Map.Entry<Integer, Integer> entry : order.entrySet()){
            JSONObject jo = new JSONObject();
            jo.put("id", entry.getKey());
            jo.put("quantity", entry.getValue());
            orderHolder.put(jo);
        }

        //Add order to request body
        jBody.put("order", orderHolder);

        Log.d(TAG, "sendOrder: sending"+jBody.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, url, jBody, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                            Log.d(TAG, "onResponse: ok");
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("LAB", error.toString());
                    }
                });
        jsonObjectRequest.setTag(TAG);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }
}
