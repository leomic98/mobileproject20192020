<?php

function db_connect() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, 'alco_db');
    
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    //echo "Connected successfully";
    return $conn;
}

function get_menu(){
    $menu = array();
    $conn = db_connect();
    $sql = "SELECT Nome FROM menu";
    $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $menu[] = $row["Nome"];
        }
    } else {
        die("error");
    }
    $conn->close();
    return $menu;

}

function get_dishes($id_menu){
    $categories = array();
    $conn = db_connect();
    $sql = "SELECT id_piatto, Nome, FK_id_categoria_piatto, Img, Prezzo, Quantita FROM piatto 
    WHERE Disponibile = 1 AND id_piatto IN (SELECT FK_id_piatto FROM piatto_in_menu WHERE FK_id_menu=".$id_menu.") 
    ORDER BY FK_id_categoria_piatto";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        $dish = array();
        $actual_category;
        while($row = $result->fetch_assoc()) {
            //se non vuole l'array associativo togliere tipo "name"=> e lasciare solo le variabili
            $ingredients_of_last_piece_in_menu = get_ingredients($row["id_piatto"]);
            $escamotage_for_last_piece_in_menu = array("name"=> $row["Nome"], "prezzo"=>$row["Prezzo"], "quantity"=>$row["Quantita"], "ingredients"=>$ingredients_of_last_piece_in_menu);
            $sql2 = "SELECT Nome FROM categoria_piatto WHERE id_categoria_piatto = ".$row["FK_id_categoria_piatto"];
            $result2 = $conn->query($sql2);
            if($result2->num_rows > 0) {
                while($row2 = $result2->fetch_assoc()){
                    if(!isset($actual_category)){ // se non e' settata la setto
                        $actual_category = $row2["Nome"];
                        $dish = array();
                        $ingredients = get_ingredients($row["id_piatto"]);
                        $dish[] = array("name"=> $row["Nome"], "prezzo"=>$row["Prezzo"], "quantity"=>$row["Quantita"], "ingredients"=>$ingredients);
                    }elseif($actual_category != $row2["Nome"]){
                        //aggiorno la lista di questa categoria con 
                        $categories[] = array($actual_category => $dish);
                        //salvo l'ultimo elemento che altrimenti andrebbe perso
                        $last_dish = end($dish);                 
                        array_pop($dish); 
                        $dish = array();
                        $ingredients = get_ingredients($row["id_piatto"]);
                        $dish[] = array("name"=> $row["Nome"], "prezzo"=>$row["Prezzo"], "quantity"=>$row["Quantita"], "ingredients"=>$ingredients);
                        //rimetto l'ultimo elemento
                        $actual_category = $row2["Nome"];
                    }else{
                        $ingredients = get_ingredients($row["id_piatto"]);
                        $dish[] = array("name"=> $row["Nome"], "prezzo"=>$row["Prezzo"], "quantity"=>$row["Quantita"], "ingredients"=>$ingredients);
                    }
                }
            } else {
                //echo("nessuna categoria trovata per questo piatto");
                return -1;
            }
        }
        //$dish[] = $escamotage_for_last_piece_in_menu;
        $categories[] = array($actual_category => $dish);
    } else {
        //echo("nessun piatto trovato per questo menu");
        return -1;
    }
    $conn->close();
    return $categories;

}

function get_ingredients($id_piatto){
    $ingredients = array();
    $conn = db_connect();
    $sql = "SELECT Nome FROM ingrediente 
    WHERE id_ingrediente IN (SELECT FK_ingrediente FROM ingrediente_in_piatto WHERE FK_piatto = ".$id_piatto.")";
    $result = $conn->query($sql);
    
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $ingredients[] = $row["Nome"];
        }
    }
    return $ingredients;
}

function get_ordering_tables(){
    $conn = db_connect();
    $tables = array();
    //Prenotato = 1 AND
    $sql = "SELECT id_tavolo FROM tavolo WHERE  id_tavolo IN(SELECT FK_id_tavolo FROM ordine)";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $tables[] = $row["id_tavolo"];
        }
    }
    return $tables;
}

function get_free_tables(){
    $conn = db_connect();
    $tables = array();
    //Prenotato = 1 AND
    $sql = "SELECT id_tavolo FROM tavolo t LEFT JOIN ordine o
    ON t.id_tavolo = o.FK_id_tavolo
    WHERE o.FK_id_tavolo IS NULL AND t.Prenotato = 0";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $tables[] = $row["id_tavolo"];
        }
    }
    return $tables;
}


function get_order($table_id){
    $conn = db_connect();
    //Prenotato = 1 AND
    $orders = null;
    $sql = "SELECT Ordine_txt FROM ordine WHERE FK_id_tavolo = ".$table_id;
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $orders[] = array($row["Ordine_txt"]);
        }
    }
    return $orders;
}

//SELECT id_ordine FROM ordine WHERE FK_id_tavolo = 2
//SELECT id_piatto_in_ordine FROM piatto_in_ordine WHERE FK_id_ordine IN (SELECT id_ordine FROM ordine WHERE FK_id_tavolo = 2)

function close_order($table_id){
    $conn = db_connect();
    //Prenotato = 1 AND
    $orders = null;
    $sql = "SELECT id_piatto_in_ordine FROM piatto_in_ordine WHERE FK_id_ordine IN (SELECT id_ordine FROM ordine WHERE FK_id_tavolo = ".$table_id.")";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $delete_stmt = "DELETE FROM piatto_in_ordine WHERE id_piatto_in_ordine = ".$row["id_piatto_in_ordine"];
            if(!$result_delete = $conn->query($delete_stmt)){
                echo("error");
                return false;
            }
        }
    }
    $sql = "SELECT id_ordine FROM ordine WHERE FK_id_tavolo =".$table_id;
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $delete_stmt = "DELETE FROM ordine WHERE id_ordine = ".$row["id_ordine"];
            if(!$result_delete = $conn->query($delete_stmt)){
                echo("error");
                return false;
            }
        }
    }
    //setta il tavolo non prenotato
    $sql = "UPDATE tavolo SET Prenotato = 0 WHERE tavolo.id_tavolo = ".$table_id;
    if(!$result =  $result = $conn->query($sql)){
        echo("error");
        return false;
    }
    return true;
}

function book_table($table_id, $number_of_people){
    ;
    $conn = db_connect();
    //Prenotato = 1 AND
    $sql = "UPDATE tavolo SET N_posti = ".$number_of_people." WHERE tavolo.id_tavolo = ".$table_id;
    $result = $conn->query($sql);
    if(!$result =  $result = $conn->query($sql)){
        echo("error");
        return false;
    }
    return true;
}
?>