package com.example.allco.summary;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.example.allco.ContainerActivity;
import com.example.allco.MenuPagerFragment;
import com.example.allco.MenuSingleton;
import com.example.allco.OrderSingleton;
import com.example.allco.R;
import com.example.allco.database.DishViewModel;
import com.example.allco.checklist.ChecklistFragment;
import com.example.allco.volleyservice.IResult;
import com.example.allco.volleyservice.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * FRAGMENT TO MANAGE THE SUMMARY OF THE DISHES THAT THE CLIENT WANTS TO ORDER, CALLED BY 'orderButton' FROM 'fragment_menu'
 */
public class SummaryFragment extends Fragment{

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ContainerActivity containerActivity;
    private List<String> dishNames = new ArrayList<>();
    private DishViewModel dishViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_summary,container,false);

        containerActivity = (ContainerActivity) getActivity();
        dishViewModel = new ViewModelProvider(containerActivity).get(DishViewModel.class);
        Set<Integer> dishIds = OrderSingleton.getInstance().getOrder().keySet();
        if (!dishIds.isEmpty()){
            for(Integer i : dishIds){
                dishViewModel.findDishByID(i).observe(containerActivity, dish -> {
                    if (dish!=null){
                        this.dishNames.add(dish.getName());
                        recAdapter.notifyDataSetChanged();
                    }
                });
            }
        }

        view.findViewById(R.id.confirmationButton).setOnClickListener(v -> {
            try {
                sendOrder(MenuSingleton.getInstance().getTableID());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        view.findViewById(R.id.backButton).setOnClickListener(v->{
            containerActivity.initFragment(new MenuPagerFragment(containerActivity), false);
        });

        initRecyclerView(view, dishNames);
        return view;
    }


    private void initRecyclerView(View v, List<String> dishNames) {
        recyclerView = (RecyclerView) v.findViewById(R.id.summaryRecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(v.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recAdapter = new SummaryAdapter(dishNames);
        recyclerView.setAdapter(recAdapter);
    }

    private void sendOrder(String tableID) throws JSONException {
        String url = "https://allco.altervista.org/order.php";

        Map<Integer, Integer> order = OrderSingleton.getInstance().getOrder();
        //Add tableID to request body
        JSONObject jBody = new JSONObject();
        jBody.put("table_id", tableID);

        //Array containing the order
        JSONArray orderHolder = new JSONArray();
        //Add the order to orderHolder
        for(Map.Entry<Integer, Integer> entry : order.entrySet()){
            JSONObject jo = new JSONObject();
            jo.put("id", entry.getKey());
            jo.put("quantity", entry.getValue());
            orderHolder.put(jo);
        }

        //Add order to request body
        jBody.put("order", orderHolder);

        Log.d("ALLCO_REQUEST_SUMMARY", "sendOrder: sending"+jBody.toString());

        VolleyService mVolleyService = new VolleyService(new IResult() {
            @Override
            public void notifyJASuccess(JSONArray response) {

            }

            @Override
            public void notifyJOSuccess(JSONObject response) {
                OrderSingleton.getInstance().addToHistory(dishNames);
                OrderSingleton.getInstance().addDishCheck(dishNames);
                OrderSingleton.getInstance().emptyOrder();
                ChecklistFragment checklistFragment = new ChecklistFragment();
                containerActivity.initFragment(checklistFragment, true);
            }

            @Override
            public void notifyError(VolleyError error) {
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(getContext(), R.string.order_error, duration);
                toast.show();
                error.printStackTrace();
            }
        }, getContext());
        mVolleyService.postJSONObjectVolley(url, jBody);
    }
}
