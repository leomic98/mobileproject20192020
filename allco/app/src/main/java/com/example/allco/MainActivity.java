package com.example.allco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.android.volley.VolleyError;
import com.example.allco.barcode.BarcodeScanningActivity;
import com.example.allco.database.Dish;
import com.example.allco.database.DishViewModel;
import com.example.allco.volleyservice.IResult;
import com.example.allco.volleyservice.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * MAIN ACTIVITY CALLS THE CAMERA TO SCAN QR CODE
 */
public class MainActivity extends AppCompatActivity {

    DishViewModel dishViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dishViewModel = new ViewModelProvider(this).get(DishViewModel.class);
        downloadDishes();
        //MenuSingleton.getInstance().setTableID("1");

        findViewById(R.id.idQrButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, BarcodeScanningActivity.class));
                //startActivity(new Intent(MainActivity.this, MenuListActivity.class));
            }
        });
    }


    /**
     * override of the backButton pressed --> the Activity is no longer destroyed when
     * the button is clicked
     */
    @Override
    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (setIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(setIntent);
        }
    }

    private void downloadDishes(){
        String URL = "https://allco.altervista.org/populate_db.php";

        VolleyService mVolleyService = new VolleyService(new IResult() {
            @Override
            public void notifyJASuccess(JSONArray response) {
                try {
                    for(int i = 0; i < response.length() ; i++){
                        JSONObject jo = response.getJSONObject(i);
                        int dishID = jo.getInt("plate_id");
                        String dishName = jo.getString("name");
                        String imageURL = jo.getString("image");
                        int price = jo.getInt("price");
                        int quantity = jo.getInt("quantity");
                        String catName = jo.getString("category_name");
                        Dish dish = new Dish(dishID, dishName, catName, imageURL, price, quantity);
                        dishViewModel.addDish(dish);
                        dishViewModel.setIsInMenu(1, dishID);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyJOSuccess(JSONObject response) { }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
            }
        }, this);
        mVolleyService.getJSONArrayVolley(URL);
    }
}
