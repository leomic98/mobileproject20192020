package com.example.allco.dishlist;

/**
 * Represents a clickable dish element
 */
public interface OnDishListener {

    /**
     * Action performed when element is clicked
     * @param pos       item position
     * @param dishID    dishID
     */
    void onDishClick(int pos, int dishID);
}
