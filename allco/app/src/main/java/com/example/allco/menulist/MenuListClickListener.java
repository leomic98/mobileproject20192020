package com.example.allco.menulist;

/**
 * Represents a clickable element inside MenuList
 */
public interface MenuListClickListener {

    /**
     * Action performed when item is clicked
     * @param pos   element position
     */
    void onListClick(int pos);
}
