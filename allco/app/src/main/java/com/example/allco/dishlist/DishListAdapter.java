package com.example.allco.dishlist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.allco.ContainerActivity;
import com.example.allco.OrderSingleton;
import com.example.allco.R;
import com.example.allco.database.Dish;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DishListAdapter extends RecyclerView.Adapter<DishListAdapter.DishCategoryViewHolder> {

    private List<Dish> dishes;
    private ContainerActivity containerActivity;
    private OnDishListener onDishListener;
    private OrderSingleton order = OrderSingleton.getInstance();

    public DishListAdapter(List<Dish> dishes, ContainerActivity containerActivity, OnDishListener onDishListener){
        this.dishes = dishes;
        this.containerActivity = containerActivity;
        this.onDishListener = onDishListener;
    }

    public static class DishCategoryViewHolder extends RecyclerView.ViewHolder {

        ImageView dishImageView;
        ImageView remQImageView;
        ImageView addQImageView;
        ImageView starImageView;
        TextView dishNameTextView;
        TextView quantityTextView;
        TextView piecesTextView;
        TextView priceTextView;
        OnDishListener dishListener;
        Integer dishID;
        private DishListAdapter dishListAdapter;
        private OrderSingleton order = OrderSingleton.getInstance();

        DishCategoryViewHolder(View view, OnDishListener onDishListener, DishListAdapter adapter){
            super(view);
            dishImageView = view.findViewById(R.id.dishImageView);
            remQImageView = view.findViewById(R.id.removeQuantityImageView);
            addQImageView = view.findViewById(R.id.addQuantityImageView);
            starImageView = view.findViewById(R.id.starImageView);
            dishNameTextView = view.findViewById(R.id.dishNameTextView);
            quantityTextView = view.findViewById(R.id.quantityTextView);
            priceTextView = view.findViewById(R.id.priceTextView);
            piecesTextView = view.findViewById(R.id.piecesTextView);
            this.dishListener = onDishListener;
            this.dishListAdapter = adapter;

            /**
             * Expands card
             */
            dishImageView.setOnClickListener(v -> dishListener.onDishClick(getAdapterPosition(), dishID));

            /**
             * Increases dish quantity in order
             */
            addQImageView.setOnClickListener(v -> {
                OrderSingleton.getInstance().addToOrder(dishID);
                quantityTextView.setText(order.getDishQuantity(dishID).toString());
                dishListAdapter.notifyDataSetChanged();
            });

            /**
             * Decreases dish quantity in order
             */
            remQImageView.setOnClickListener(v -> {
                OrderSingleton.getInstance().decFromOrder(dishID);
                quantityTextView.setText(order.getDishQuantity(dishID).toString());
                dishListAdapter.notifyDataSetChanged();
            });
        }
    }

    @NonNull
    @Override
    public DishCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_card, parent, false);
        return new DishCategoryViewHolder(view, onDishListener, this);
    }

    @Override
    public void onBindViewHolder(@NonNull DishCategoryViewHolder holder, int position) {

        Dish dish = dishes.get(position);
        holder.dishID = dish.getId();
        if (!dish.isInfoInitialized()){
            holder.piecesTextView.append(" " + dish.getQuantity());
            holder.priceTextView.append(" " + dish.getPrice());
            dish.setInfoInitialized(true);
        }
        holder.dishNameTextView.setText(dish.getName());
        holder.quantityTextView.setText(order.getDishQuantity(dish.getId()).toString());
        Picasso.get()
                .load(dish.getImage())
                .resize(100, 100)
                .centerCrop()
                .into(holder.dishImageView);
        if(dish.isFavourite()){
            holder.starImageView.setImageResource(R.drawable.star);
        }
        holder.starImageView.setOnClickListener(new FavouriteListener(holder.starImageView,
                dish,containerActivity, this));
    }

    @Override
    public int getItemCount() {
        return dishes.size();
    }
}
