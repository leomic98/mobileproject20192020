package com.example.allco;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the chosen Menu
 */
public class MenuSingleton {

    private static MenuSingleton instance = null;

    private List<String> categories;
    private Integer menuID;
    private String tableID;

    private MenuSingleton(){
        this.categories = new ArrayList<>();
    }

    public static  MenuSingleton getInstance(){
        if (instance == null){
            synchronized (MenuSingleton.class) {
                if (instance == null) {
                    instance = new MenuSingleton();
                }
            }
        }
        return instance;
    }

    public List<String> getCategories(){
        return categories;
    }

    public void addCategory(final String catName){
        if(!checkCategory(catName)){
            this.categories.add(catName);
        }
    }

    public Integer getMenuID() {
        return menuID;
    }

    public void setMenuID(Integer menuID) {
        this.menuID = menuID;
    }

    public void setTableID(String tableID) {
        this.tableID = tableID;
    }

    public String getTableID() {
        return tableID;
    }

    public void emptyCategories(){
        this.categories.clear();
    }

    public boolean checkCategory(String catName){
        for (String s: categories){
            if(s.equals(catName)){
                return true;
            }
        }
        return false;
    }
}
