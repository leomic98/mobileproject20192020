package com.example.allco.menulist;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.example.allco.ContainerActivity;
import com.example.allco.MenuSingleton;
import com.example.allco.OrderSingleton;
import com.example.allco.R;
import com.example.allco.database.DishViewModel;
import com.example.allco.volleyservice.IResult;
import com.example.allco.volleyservice.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * ACTIVITY THAT MANAGES THE TYPES OF MENUS THAT ARE AVAILABLE, ONCE THE CLIENT CLICK ON ONE OF THEM,
 * IT REDIRECTS TO THE SINGLE MENU HE HAD CHOOSEN, MANAGED BY 'ContainerFragment' > 'SingleMenuFragment'
 */
public class MenuListActivity extends AppCompatActivity implements MenuListClickListener {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private MenuSingleton menu = MenuSingleton.getInstance();
    private OrderSingleton order = OrderSingleton.getInstance();

    private final static String TAG = "ALLCO_USR_REQUEST";

    //A list containing all menu types
    private Map<Integer, String> menuTypes = new HashMap<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_list);

        //updateMenuTypes(String.valueOf(1));
        updateMenuTypes(menu.getTableID());
        initRecyclerView();
    }

    private void initRecyclerView(){
        recyclerView = (RecyclerView) findViewById(R.id.menuTypesRecylcerView);
        layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(layoutManager);
        recAdapter = new MenuListAdapter(menuTypes, this);
        recyclerView.setAdapter(recAdapter);
    }

    @Override
    public void onListClick(int pos) {
        menu.setMenuID(pos);
        clearDB();
        startActivity(new Intent(this, ContainerActivity.class));
    }

    private void clearDB(){
        if (!menu.getCategories().isEmpty()){
            menu.emptyCategories();
        }
        if(!order.getOrder().isEmpty()){
            order.emptyOrder();
        }
        DishViewModel dishViewModel = new ViewModelProvider(this).get(DishViewModel.class);
        dishViewModel.clearDishesInMenu();
    }
    /**
     * Updates the menu types data set, represented by a List<String>
     */
    private void updateMenuTypes(String tableID){
        String url = "https://allco.altervista.org/get_menu.php?table_id="+tableID;

        VolleyService mVolleyService = new VolleyService(new IResult() {
            @Override
            public void notifyJASuccess(JSONArray response) {
                try {
                    for(int i = 0; i < response.length() ; i++){
                        JSONObject jo = response.getJSONObject(i);
                        Integer menuID = jo.getInt("menu_id");
                        String menuType = jo.getString("type");
                        if(!menuTypes.containsKey(menuID)){
                            menuTypes.put(menuID, menuType);
                            recAdapter.notifyItemInserted(menuTypes.size()-1);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyJOSuccess(JSONObject response) { }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
            }
        }, this);
        mVolleyService.getJSONArrayVolley(url);
    }
}
