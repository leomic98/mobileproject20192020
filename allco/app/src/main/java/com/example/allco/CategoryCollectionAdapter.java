package com.example.allco;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.allco.dishlist.DishListFragment;

import java.util.List;

public class CategoryCollectionAdapter extends FragmentStateAdapter {
    private List<String> categories;

    public CategoryCollectionAdapter(@NonNull Fragment fragment) {
        super(fragment);
        categories = MenuSingleton.getInstance().getCategories();
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        String catName = categories.get(position);
        return new DishListFragment(catName);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}
