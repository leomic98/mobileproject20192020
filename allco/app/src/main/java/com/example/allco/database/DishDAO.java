package com.example.allco.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DishDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addDish(Dish dish);

    @Query("SELECT * FROM dish_saved  WHERE is_in_menu = 1 ORDER BY dish_id")
    LiveData<List<Dish>> getDishes();

    @Query("SELECT * FROM dish_saved WHERE dish_id LIKE :id AND is_in_menu = 1")
    LiveData<Dish> findDishByID(int id);

    @Query("DELETE FROM dish_saved WHERE dish_id LIKE :id")
    void removeDish(int id);

    @Query("SELECT * FROM dish_saved WHERE is_favourite = 1 AND is_in_menu = 1 ORDER BY dish_id ASC")
    LiveData<List<Dish>> getFavourites();

    @Query("SELECT * FROM dish_saved WHERE category_name LIKE :catName  AND is_in_menu = 1 ORDER BY dish_id ASC")
    LiveData<List<Dish>> getDishesInCategory(String catName);

    @Query("UPDATE dish_saved SET is_favourite = :favouritee WHERE dish_id LIKE :dishID")
    void setFavouritee(int favouritee, int dishID);

    @Query("DELETE FROM dish_saved WHERE is_favourite = 0")
    void clear();

    @Query("UPDATE dish_saved SET is_in_menu = :manu WHERE dish_id LIKE :dishID")
    void setIsInMenu(int manu, int dishID);

    @Query("UPDATE dish_saved SET is_in_menu = 0")
    void clearDishesInMenu();
}
