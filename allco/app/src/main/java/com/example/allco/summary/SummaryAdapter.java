package com.example.allco.summary;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.allco.OrderSingleton;
import com.example.allco.R;

import java.util.List;

public class SummaryAdapter extends RecyclerView.Adapter<SummaryAdapter.SummaryViewHolder> {

    private List<String> dishNames;


    public SummaryAdapter(List<String> mDishNames) {
        this.dishNames = mDishNames;
    }

    public static class SummaryViewHolder extends RecyclerView.ViewHolder{

        TextView tv1;
        TextView tv2;

        public SummaryViewHolder(View view) {
            super(view);
            this.tv1 = view.findViewById(R.id.nameSummaryTextView);
            this.tv2 = view.findViewById(R.id.quantitySummaryTextView);
        }
    }

    @NonNull
    @Override
    public SummaryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.summary_row, parent, false);
        SummaryViewHolder vh = new SummaryViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull SummaryAdapter.SummaryViewHolder holder, int position) {
        holder.tv1.setText(dishNames.get(position));
        Integer dishQuantity = (Integer) OrderSingleton.getInstance().getOrder().values().toArray()[position];
        holder.tv2.setText(String.valueOf(dishQuantity));
    }

    @Override
    public int getItemCount() {
        return dishNames.size();
    }
}
