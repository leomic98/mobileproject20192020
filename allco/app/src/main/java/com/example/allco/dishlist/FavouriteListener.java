package com.example.allco.dishlist;

import android.view.View;
import android.widget.ImageView;

import androidx.lifecycle.ViewModelProvider;

import com.example.allco.ContainerActivity;
import com.example.allco.R;
import com.example.allco.database.Dish;
import com.example.allco.database.DishViewModel;

public class FavouriteListener implements View.OnClickListener {

    private ImageView img;
    private Dish dish;
    private DishViewModel dishViewModel;
    private DishListAdapter dishListAdapter;
    private static final int TRUE = 1;
    private static final int FALSE = 0;

    public FavouriteListener(ImageView img, Dish dish,
                             ContainerActivity containerActivity, DishListAdapter adapter){
        this.img = img;
        this.dish = dish;
        this.dishViewModel = new ViewModelProvider(containerActivity).get(DishViewModel.class);
        this.dishListAdapter = adapter;
    }

    @Override
    public void onClick(View v) {
        if(this.dish.isFavourite()){
            dish.setFavourite(false);
            dishViewModel.setFavouritee(FALSE, dish.getId());
            img.setImageResource(R.drawable.star_empty);
            dishListAdapter.notifyDataSetChanged();
        }else{
            dish.setFavourite(true);
            dishViewModel.setFavouritee(TRUE, dish.getId());
            img.setImageResource(R.drawable.star);
            dishListAdapter.notifyDataSetChanged();
        }
    }
}
