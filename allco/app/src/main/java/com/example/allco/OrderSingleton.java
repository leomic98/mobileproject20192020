package com.example.allco;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents the order
 */
public class OrderSingleton {

    private static OrderSingleton instance = null;
    private Map<Integer, Integer> order;
    private List<String> history;
    private List<Boolean> dishCheck;


    public OrderSingleton(){
        this.order = new HashMap<>();
        this.history = new ArrayList<>();
        this.dishCheck = new ArrayList<>();
    }

    public static  OrderSingleton getInstance(){
        if (instance == null){
            synchronized (OrderSingleton.class) {
                if (instance == null) {
                    instance = new OrderSingleton();
                }
            }
        }
        return instance;
    }

    public void addToOrder(Integer id){
        if(order.get(id) != null) {
            Integer quantity = order.get(id);
            quantity++;
            order.put(id, quantity);
        }else {
            order.put(id, 1);
        }
    }

    public void decFromOrder(Integer id){
        if(order.get(id) != null) {
            Integer quantity = order.get(id);
            if (quantity > 1) {
                quantity--;
                order.put(id, quantity);
            } else {
                order.remove(id);
            }
        }
    }

    public Integer getDishQuantity(Integer id){
        if(order.get(id) != null){
            return order.get(id);
        }
        return 0;
    }

    public void emptyOrder(){
        order.clear();
    }

    public Map<Integer,Integer> getOrder(){
        return this.order;
    }

    public void addToHistory(List<String> order){
        this.history.addAll(order);
    }

    public List<String> getHistory(){
        return this.history;
    }

    public List<Boolean> getDishCheck() {
        return dishCheck;
    }

    public void addDishCheck(List<String> dishes){
        for (String a : dishes) {
            this.dishCheck.add(false);
        }
    }

    public void setDishCheck(int pos, boolean check) {
        this.dishCheck.set(pos,check);
    }
}
