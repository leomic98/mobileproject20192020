package com.example.allco.checklist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.allco.ContainerActivity;
import com.example.allco.MainActivity;
import com.example.allco.MenuPagerFragment;
import com.example.allco.OrderSingleton;
import com.example.allco.R;
import com.example.allco.database.DishViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * FRAGMENT TO MANAGE 'fragment_checklist', USED AFTER THE CLIENT HAS ORDERED TO CHECK HIS DISHES
 */
public class ChecklistFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ContainerActivity containerActivity;
    private List<String> dishNames = new ArrayList<>();
    private DishViewModel dishViewModel;
    private List<Boolean> dishCheck = new ArrayList<>();
    private OrderSingleton order = OrderSingleton.getInstance();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_checklist,container,false);

        dishViewModel = new ViewModelProvider(getActivity()).get(DishViewModel.class);
        containerActivity = (ContainerActivity) getActivity();
        dishNames = order.getHistory();
        dishCheck = order.getDishCheck();

        view.findViewById(R.id.backToMenuButton).setOnClickListener(e->{
            containerActivity.initFragment(new MenuPagerFragment(containerActivity), false);
        });

        view.findViewById(R.id.finishButton).setOnClickListener(e->{
            showFinishAlertDialog();
        });

        initRecyclerView(view);
        return view;
    }


    private void initRecyclerView(View v) {
        recyclerView = (RecyclerView) v.findViewById(R.id.dishesStatusRecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(v.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recAdapter = new ChecklistAdapter(dishNames, dishCheck);
        recyclerView.setAdapter(recAdapter);
    }

    private void showFinishAlertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setPositiveButton(R.string.yes, (dialog, id) -> {
            dishViewModel.clearDishesInMenu();
            order.emptyOrder();
            order.getHistory().clear();
            containerActivity.changeActivity(getContext(), MainActivity.class);
        });
        builder.setNegativeButton(R.string.no, (dialog, id) -> {
            dialog.dismiss();
        });

        builder.setTitle(R.string.have_finished);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
