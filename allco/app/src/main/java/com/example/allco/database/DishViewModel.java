package com.example.allco.database;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class DishViewModel extends AndroidViewModel {

    private DishRepository repository;

    public DishViewModel(@NonNull Application application){
        super(application);
        repository = new DishRepository(application);

    }

    public LiveData<Dish> findDishByID(int dishID){
        LiveData<Dish> dishByID = repository.findDishByID(dishID);
        return dishByID;
    }

    public LiveData<List<Dish>> getDishesInCategory(String catName){
        return repository.getDishesInCategory(catName);
    }

    public void setFavouritee(int favouritee, int dishID){
        repository.setFavouritee(favouritee, dishID);
    }

    public void setIsInMenu(int manu, int dishID){
        repository.setIsInMenu(manu, dishID);
    }

    public void addDish(Dish dish){
        repository.addDish(dish);
    }

    public LiveData<List<Dish>> getFavourites() {
        return repository.getFavourites();
    }

    public void clearDishesInMenu(){
        repository.clearDishesInMenu();
    };
}
