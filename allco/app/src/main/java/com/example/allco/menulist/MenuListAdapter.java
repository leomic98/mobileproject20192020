package com.example.allco.menulist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.allco.R;

import java.util.Map;

/**
 * MANAGES THE BUILDING OF THE BUTTONS FOR THE RECYCLER VIEW IN 'activity_menu_list' THE TYPES OF THE MENUS3
 */
public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.ListViewHolder> {

    private Map<Integer,String> menuTypes;
    private MenuListClickListener menuListClickListener;

    public MenuListAdapter(Map<Integer,String> menuTypes, MenuListClickListener menuListClickListener){
        this.menuTypes = menuTypes;
        this.menuListClickListener = menuListClickListener;
    }

    public static class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        Button bt;
        int menuID;
        MenuListClickListener menuListClickListener;

        public ListViewHolder(Button button, MenuListClickListener menuListClickListener) {
            super(button);
            this.bt = button;
            this.menuListClickListener = menuListClickListener;
            bt.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            menuListClickListener.onListClick(menuID);
        }
    }



    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Button button = (Button) LayoutInflater.from(parent.getContext()).inflate(R.layout.category_button, parent,false);
        ListViewHolder viewHolder = new ListViewHolder(button, menuListClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        Integer menuID = (Integer) menuTypes.keySet().toArray()[position];
        holder.bt.setText(menuTypes.get(menuID));
        holder.menuID = menuID;
    }

    @Override
    public int getItemCount() {
        return menuTypes.size();
    }
}
