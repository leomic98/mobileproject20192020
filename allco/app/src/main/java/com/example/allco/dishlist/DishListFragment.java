package com.example.allco.dishlist;

import android.os.Bundle;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.example.allco.ContainerActivity;
import com.example.allco.R;
import com.example.allco.database.Dish;
import com.example.allco.database.DishViewModel;
import com.example.allco.volleyservice.IResult;
import com.example.allco.volleyservice.VolleyService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DishListFragment extends Fragment implements OnDishListener{

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private String categoryName;

    public DishListFragment(String categoryName){
        this.categoryName = categoryName;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dish_list,container,false);

        DishViewModel dishViewModel = new ViewModelProvider(getActivity()).get(DishViewModel.class);

        Log.d("dishList", "onCreateView: "+ categoryName);
        if(categoryName.equals("preferiti")){
            dishViewModel.getFavourites().observe(getActivity(), dishes -> {
                List<Dish> list = new ArrayList<>();
                for (Dish dish : dishes) {
                    list.add(dish);
                }
                initRecyclerView(view, list);
            });
        } else {
            dishViewModel.getDishesInCategory(categoryName).observe(getActivity(), dishes -> {
                List<Dish> list = new ArrayList<>();
                for (Dish dish : dishes) {
                    list.add(dish);
                }
                initRecyclerView(view, list);
            });
        }
        return view;
    }

    private void initRecyclerView(View v, List<Dish> dishes) {
        recyclerView = (RecyclerView) v.findViewById(R.id.rowRecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(v.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recAdapter = new DishListAdapter(dishes, (ContainerActivity) getActivity(), this);
        recyclerView.setAdapter(recAdapter);
    }

    @Override
    public void onDishClick(int pos, int dishID){
        final View view = Objects.requireNonNull(recyclerView.findViewHolderForAdapterPosition(pos)).itemView;
        final CardView cardView = view.findViewById(R.id.cardView);
        final View expandableView = cardView.findViewById(R.id.expandableView);

        TextView ingredientsView = view.findViewById(R.id.ingredientsTextView);
        if(ingredientsView.getText().toString().equals("")){
            downloadIngredients(dishID, ingredientsView);
        }

        TransitionManager.beginDelayedTransition(cardView, new Slide());
        expandableView.setVisibility(expandableView.getVisibility()== android.view.View.GONE ?
                android.view.View.VISIBLE : android.view.View.GONE );
    }

    private void downloadIngredients(int dishID, TextView ingredientsView){
        String URL = "https://allco.altervista.org/get_ingredients.php?dish_id="+dishID;

        VolleyService mVolleyService = new VolleyService(new IResult() {
            @Override
            public void notifyJASuccess(JSONArray response){
                try {
                    for(int i = 0; i < response.length() ; i++){
                        JSONObject jo = response.getJSONObject(i);
                        String ingredient = jo.getString("name");
                        ingredientsView.append("\n"+ingredient);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyJOSuccess(JSONObject response) { }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
            }
        }, getContext());
        mVolleyService.getJSONArrayVolley(URL);
    }
}
