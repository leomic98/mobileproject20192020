package com.example.allco.database;

import android.graphics.Bitmap;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "dish_saved")
public class Dish {
    @PrimaryKey
    @ColumnInfo(name="dish_id")
    private int id;

    @ColumnInfo(name = "dish_name")
    private String name;

    @ColumnInfo(name="category_name")
    private String categoryName;

    @ColumnInfo(name = "img")
    private String image;

    @ColumnInfo(name="price")
    private int price;

    @ColumnInfo(name = "quantity")
    private int quantity;

    @ColumnInfo(name =  "is_favourite")
    private boolean isFavourite;

    @ColumnInfo(name= "is_in_menu")
    private boolean isInManu;

    @Ignore
    private Bitmap bitmap;

    @Ignore
    private boolean isInfoInitialized = false;

    public Dish(int id, String name, String categoryName, String image, int price, int quantity){
        this.id = id;
        this.name = name;
        this.categoryName = categoryName;
        this.image = image;
        this.price = price;
        this.quantity = quantity;
        this.isInManu = true;
        this.isFavourite = false;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getImage() {
        return image;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public boolean isInManu() {
        return isInManu;
    }

    public void setInManu(boolean inManu) {
        isInManu = inManu;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public void setInfoInitialized(boolean infoInitialized) {
        isInfoInitialized = infoInitialized;
    }

    public boolean isInfoInitialized() {
        return isInfoInitialized;
    }
}
