package com.example.allco;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


public class ContainerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);

        initFragment(new MenuPagerFragment(this), false);
    }

    public void initFragment(Fragment newFragment, boolean isBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack("BACKSTACK_BASE", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        transaction.replace(R.id.fragmentContainer, newFragment);
        if(isBackStack){
            transaction.addToBackStack("BACKSTACK_BASE");
        }
        transaction.commit();
    }

    public void changeActivity(Context context, Class<?> activity){
        startActivity(new Intent(context, activity));
    }
}
