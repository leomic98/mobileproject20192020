package com.example.allco.database;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class DishRepository {
    private DishDAO dishDAO;

    public DishRepository(Application application){
        DishRoomDatabase db = DishRoomDatabase.getDatabase(application);
        dishDAO=db.dishDAO();
    }

    public LiveData<Dish> findDishByID(int dishID){
        return dishDAO.findDishByID(dishID);
    }

    public void addDish(final Dish dish){
        DishRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                dishDAO.addDish(dish);
            }
        });
    }

    public void removeDish(int dishID){
        DishRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                dishDAO.removeDish(dishID);
            }
        });
    }

    public LiveData<List<Dish>> getFavourites(){
        LiveData<List<Dish>> favourites = dishDAO.getFavourites();
        return favourites;
    }

    public LiveData<List<Dish>> getDishesInCategory(String catName){
        LiveData<List<Dish>> dishesInCategory = dishDAO.getDishesInCategory(catName);
        return dishesInCategory;
    }

    public void setFavouritee(int favouritee, int dishID){
        DishRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                dishDAO.setFavouritee(favouritee, dishID);
            }
        });
    }

    public void setIsInMenu(int manu, int dishID){
        DishRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                dishDAO.setIsInMenu(manu, dishID);
            }
        });
    }

    public void clear(){
        DishRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                dishDAO.clear();
            }
        });
    }

    public void clearDishesInMenu(){
        DishRoomDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                dishDAO.clearDishesInMenu();
            }
        });
    }
}
