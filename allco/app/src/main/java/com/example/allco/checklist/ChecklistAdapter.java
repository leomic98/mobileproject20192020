package com.example.allco.checklist;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.allco.OrderSingleton;
import com.example.allco.R;

import java.util.List;

public class ChecklistAdapter extends RecyclerView.Adapter<ChecklistAdapter.ChecklistViewHolder> {

    private OrderSingleton order = OrderSingleton.getInstance();
    private List<String> orderedDishes;
    private List<Boolean> dishCheck;

    public ChecklistAdapter(List<String> orderedDishes, List<Boolean> dishCheck) {
        this.orderedDishes = orderedDishes;
        this.dishCheck = dishCheck;
    }

    public static class ChecklistViewHolder extends RecyclerView.ViewHolder{

        CheckBox checkBox;
        private List<Boolean> dishCheck;

        public ChecklistViewHolder(LinearLayout linearLayout, CheckBox checkBox) {
            super(linearLayout);
            this.checkBox = checkBox;
            dishCheck = OrderSingleton.getInstance().getDishCheck();
        }
    }

    @NonNull
    @Override
    public ChecklistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.dish_checkbox, parent, false);
        CheckBox checkBox = linearLayout.findViewById(R.id.dishesStatusCheckBox);

        ChecklistViewHolder vh = new ChecklistViewHolder(linearLayout, checkBox);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ChecklistViewHolder holder, int position) {
        holder.checkBox.setText(orderedDishes.get(position));
        holder.checkBox.setOnClickListener(view -> {
            if(order.getDishCheck().get(position)){
                order.setDishCheck(position,false);
            }else{
                order.setDishCheck(position,true);
            }
        });
        if(order.getDishCheck().get(position)){
            holder.checkBox.setChecked(true);
        }else{
            holder.checkBox.setChecked(false);
        }
    }

    @Override
    public int getItemCount() {
        return orderedDishes.size();
    }
}

