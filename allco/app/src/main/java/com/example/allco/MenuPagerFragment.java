package com.example.allco;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import com.android.volley.VolleyError;
import com.example.allco.database.Dish;
import com.example.allco.database.DishViewModel;
import com.example.allco.checklist.ChecklistFragment;
import com.example.allco.summary.SummaryFragment;
import com.example.allco.volleyservice.IResult;
import com.example.allco.volleyservice.VolleyService;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MenuPagerFragment extends Fragment {

    private CategoryCollectionAdapter categoryCollectionAdapter;
    private ViewPager2 viewPager;
    private MenuSingleton menu = MenuSingleton.getInstance();
    private int menuID = menu.getMenuID();
    private DishViewModel dishViewModel;
    private ContainerActivity containerActivity;

    public MenuPagerFragment(ContainerActivity containerActivity){
        this.containerActivity = containerActivity;
        dishViewModel = new ViewModelProvider(containerActivity).get(DishViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
         View v = inflater.inflate(R.layout.fragment_menu_pager, container, false);
        if(menu.getCategories().isEmpty()|| (menu.getCategories().get(0).equals("preferiti")&& menu.getCategories().size()==1 )) {
            downloadCategories(menuID);
            updateAvailability(menuID);
        }
        dishViewModel.getFavourites().observe(getActivity(), dishes -> {
            List<Dish> list = new ArrayList<>();
            for (Dish dish : dishes) {
                list.add(dish);
            }
            if(!list.isEmpty()){
                if(!menu.checkCategory("preferiti")){
                    menu.getCategories().add("preferiti");
                    categoryCollectionAdapter.notifyDataSetChanged();
                }
            }
        });
        v.findViewById(R.id.goToStatusButton).setOnClickListener(e->{
            if (!OrderSingleton.getInstance().getHistory().isEmpty()){
                containerActivity.initFragment(new ChecklistFragment(), true);
            }else{
                showToast(getString(R.string.status_warning));
            }
        });

        v.findViewById(R.id.orderButton).setOnClickListener(e -> {
            if(OrderSingleton.getInstance().getOrder().isEmpty()){
                showToast(getString(R.string.order_warning));
            } else {
                containerActivity.initFragment(new SummaryFragment(), true);
            }
        });
        return  v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        categoryCollectionAdapter= new CategoryCollectionAdapter(this);
        viewPager = view.findViewById(R.id.menuPager);
        viewPager.setAdapter(categoryCollectionAdapter);

        TabLayout tabLayout = view.findViewById(R.id.tabLayout);

        new TabLayoutMediator(tabLayout, viewPager,
                (tab, position) -> tab.setText(menu.getCategories().get(position))
        ).attach();
    }

   private void downloadCategories(int menuID){
       String URL = "https://allco.altervista.org/get_category_names.php?menu_id="+menuID;

       VolleyService mVolleyService = new VolleyService(new IResult() {
           @Override
           public void notifyJASuccess(JSONArray response) {
               try {
                   for(int i = 0; i < response.length() ; i++){
                       JSONObject jo = response.getJSONObject(i);
                       String catName = jo.getString("name");
                       menu.addCategory(catName);
                       categoryCollectionAdapter.notifyItemInserted(menu.getCategories().size()-1);
                   }

               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }

           @Override
           public void notifyJOSuccess(JSONObject response) { }

           @Override
           public void notifyError(VolleyError error) {
               error.printStackTrace();
           }
       }, getContext());
       mVolleyService.getJSONArrayVolley(URL);
   }


    //prova di aggiornamento parziale
    private void updateAvailability(int menuID){
        String URL = "https://allco.altervista.org/get_dishes_when_present.php?menu_id="+menuID;

        VolleyService mVolleyService = new VolleyService(new IResult() {
            @Override
            public void notifyJASuccess(JSONArray response) {
                try {
                    for(int i = 0; i < response.length() ; i++){
                        JSONObject jo = response.getJSONObject(i);
                        int dishID = jo.getInt("plate_id");
                        dishViewModel.setIsInMenu(1, dishID);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyJOSuccess(JSONObject response) { }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
            }
        }, getContext());
        mVolleyService.getJSONArrayVolley(URL);
    }

    private void showToast(final String msg){
        int toastXoff = 0, toastYoff = 0;
        int duration = Toast.LENGTH_SHORT;
        Context context = getContext();
        if (context != null){
            Toast toast = Toast.makeText(context, msg, duration);
            toast.setGravity(Gravity.CENTER, toastXoff, toastYoff);
            toast.show();
        }
    }
}
